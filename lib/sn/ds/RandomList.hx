package sn.ds;

@:forward
abstract RandomList<T>(Array<T>) {

	inline public function new(list:Array<T>) {
		this = list.copy();
		shuffle();
	}

	/**
	 * shuffle the items randomly
	 */
	inline public function shuffle() {
		// taken from hxrandom
		// https://github.com/jasononeil/hxrandom/blob/master/src/Random.hx
		for (i in 0...this.length) {
			var j = Math.floor(Math.random() * this.length);
			var a = this[i];
			var b = this[j];
			this[i] = b;
			this[j] = a;
		}
	}

	/**
	 * take a random element from the list
	 */
	inline public function take() : T {
		var index = Math.floor(Math.random() * this.length);
		var item = this[index];
		this.remove(item);
		return item;
	}

}
