package sn.ds;

class RomanNumerals {

	// [TODO) this needs to be cleaned up, the make function
	// doesn't need to exist anymore..

	public static function convert(n:Int) : String {
		var originaln = n;
		var numeral = "";

		var m_count = count(n,1000);
		n = n - 1000 * m_count;
		numeral += make(m_count, "M");

		var d_count = count(n, 500);
		n = n - 500 * d_count;
		numeral += make(d_count, "D");

		var c_count = count(n, 100);
		n = n - 100 * c_count;
		numeral += make(c_count, "C");

		var l_count = count(n, 50);
		n = n - 50 * l_count;
		numeral += make(l_count, "L");
		if ((n - 40 >= 0) && (n-40 <= 9)) {
			n = n - 40;
			numeral += "XL";
		}

		var x_count = count(n, 10);
		n = n - 10 * x_count;
		numeral += make(x_count, "X");
		if (n == 9) {
			n = n - 9;
			numeral += "IX";
		}

		var v_count = count(n, 5);
		n = n - 5 * v_count;
		numeral += make(v_count, "V");
		if (n == 4) {
			n = n - 4;
			numeral += "IV";
		}

		var i_count = count(n, 1);
		n = n - i_count;
		numeral += make(i_count, "I");

		return numeral;
	}

	/**
	 * counts how many times the number is divisible
	 */
	inline private static function count(number, divisor) : Int {
		return Math.floor(number/divisor);
	}

	/**
	 * creates the graphic for the numeral,
	 */
	inline private static function make(count:Int, char:String) : String {
 		var numeral = "";
		while(count > 0) {
			numeral += char;
			count -= 1;
		};
		return numeral;
	}
}

#if Test

class RomanNumeralsTest extends utest.Test {

	public function testRomanNumerals() {
		utest.Assert.equals("I", RomanNumerals.convert(1));
		utest.Assert.equals("II", RomanNumerals.convert(2));
		utest.Assert.equals("III", RomanNumerals.convert(3));
		utest.Assert.equals("IV", RomanNumerals.convert(4));
		utest.Assert.equals("V", RomanNumerals.convert(5));
		utest.Assert.equals("VI", RomanNumerals.convert(6));
		utest.Assert.equals("VII", RomanNumerals.convert(7));
		utest.Assert.equals("VIII", RomanNumerals.convert(8));
		utest.Assert.equals("IX", RomanNumerals.convert(9));
		utest.Assert.equals("X", RomanNumerals.convert(10));
		utest.Assert.equals("XIV", RomanNumerals.convert(14));
		utest.Assert.equals("XXIII", RomanNumerals.convert(23));
		utest.Assert.equals("XXIX", RomanNumerals.convert(29));
		utest.Assert.equals("XLIII", RomanNumerals.convert(43));
		utest.Assert.equals("LXXX", RomanNumerals.convert(80));
	}

}

#end
