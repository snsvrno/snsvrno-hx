package sn.ds;

class ArrayTools {

	/**
	 * puts `array2` into `array1` at the end, modifying `array1` in place.
	 */
	public static function merge<T>(array1 : Array<T>, array2 : Array<T>) {
		for (a in array2) array1.push(a);
	}
}

#if Test
class ArrayToolsTest extends utest.Test {

	public function testMerge() {
		var a1 = [1,2,3,4,5,6,7,8,9,10];
		var a2 = [11,12,13,14,15,16,17,18,19,20];

		ArrayTools.merge(a1, a2);
		utest.Assert.equals(20, a1.length);
		for (i in 0 ... a1.length) {
			utest.Assert.equals(i+1, a1[i]);
		}
	}

}
#end
