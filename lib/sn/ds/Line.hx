package sn.maths;

class Line {

	public var p1 : Point;
	public var p2 : Point;

	public var slope : Float;
	public var intercept : Float;

	public function new(x1 : Float, y1 : Float, x2 : Float, y2 : Float) {
		p1 = new Point(x1, y1);
		p2 = new Point(x2, y2);
	}

	public function length() : Float
		return Math.sqrt(Math.pow(p2.y-p1.y,2) + Math.pow(p2.x-p1.x,2));

	public function pointOnLineDistance(distance : Float) : Point 
		return pointOnLine(distance / length());

	public function pointOnLine(percent : Float) : Point {
		if (percent >= 1) return p2;
		else if (percent <= 0) return p1;
		else {

			var dx = p2.x - p1.x;
			var dy = p2.y - p1.y;

			return new Point(
				p1.x + dx * percent,
				p1.y + dy * percent
			);

		}
	}

	public function midpoint() : Point return pointOnLine(0.5);


	public function drawPoint(graphic : h2d.Graphics, distance : Float, color : Int, size : Float, xoffset : Float, yoffset : Float) {
		
		var m = pointOnLineDistance(distance);
		graphic.beginFill(color);
		graphic.drawRect(m.x - size/2 + xoffset, m.y - size/2 + yoffset, size, size);
		graphic.endFill();
	}

	#if debug
	public function d_draw(graphic : h2d.Graphics, xoffset : Float, yoffset : Float) {
		graphic.lineStyle(2, 0x00FF44);

		graphic.moveTo(p1.x + xoffset, p1.y + yoffset);
		graphic.lineTo(p2.x + xoffset, p2.y + yoffset);
	}

	public function d_drawmid(graphic : h2d.Graphics, xoffset : Float, yoffset : Float, ?color : Int = 0x00FF44) {
		
		var m = midpoint();
		graphic.beginFill(color);
		graphic.drawRect(m.x - 5 + xoffset, m.y - 5 + yoffset, 10, 10);
		graphic.endFill();
	}
	#end
}
