package sn.ds;

class StructureTools {

	/**
	 * Fills the `object` with entries from `defaults` if doen't
	 * already exist.
	 */
	public static function fillFromDefaults(object:Dynamic, defaults:Dynamic) {
		for (key in Reflect.fields(defaults)) {
			var value = Reflect.getProperty(defaults, key);

			// is a dictonary
			if (Reflect.isObject(value) && !(value is String)) {

				var obj = Reflect.getProperty(object, key);
				if (obj == null) {
					obj = { };
					Reflect.setProperty(object, key, obj);
				}
				fillFromDefaults(obj, value);
			}

			// is a value
			else {
				if (!Reflect.hasField(object, key))
					Reflect.setProperty(object, key, value);
			}

			if (!Reflect.hasField(object, key)) {
				
			}
		}
	}

}
