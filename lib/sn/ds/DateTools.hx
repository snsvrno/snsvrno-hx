package sn.ds;

#if Test import Test.equalsF; #end

class DateTools {

	/**
	 * calculates the differnce between two date times in hours
	 */
	public static function between(a:Date, b:Date) : Float {

		// getting the hourly difference just using the time
		var timediff = (
			a.getHours() + a.getMinutes() / 60 + a.getSeconds() / 60 / 60
		) - (
			b.getHours() + b.getMinutes() / 60 + b.getSeconds() / 60 / 60
		);

		// getting the date diff
		var datediff = daysBetween(a, b) * 24;

		return datediff + timediff;
	}

	/**
	 * counts the days in a year
	 */
	public static function daysInYear(year:Int) : Int {

		var days = 0;
		for (month in 0 ... 12) {
			days += std.DateTools.getMonthDays(
				make(year, month, 1, 1, 1, 1)
			);
		}
		return days;
	}

	/**
	 * creates a `Date` object from `Int`s
	 */
	public static function make(year:Int, month:Int, day:Int, hour:Int, minute:Int, second:Int) : Date {
		var monthStr = StringTools.padNumber(month, 2);
		var dayStr = StringTools.padNumber(day, 2);
		var hourStr = StringTools.padNumber(hour, 2);
		var minuteStr = StringTools.padNumber(minute, 2);
		var secondStr = StringTools.padNumber(second, 2);
		return Date.fromString('$year-$monthStr-$dayStr $hourStr:$minuteStr:$secondStr');
	}

	/**
	 * counts the days between 2 different dates
	 */
	public static function daysBetween(a:Date, b:Date) : Int {
		var adays = 0;
		for (month  in 0 ... a.getMonth() + 1) {
			adays += std.DateTools.getMonthDays(make(
				a.getFullYear(), month, a.getDate(), 
				a.getHours(), a.getMinutes(), a.getSeconds()
			));
		}

		var bdays = 0;
		for (month  in 0 ... b.getMonth() + 1) {
			bdays += std.DateTools.getMonthDays(make(
				b.getFullYear(), month, b.getDate(), 
				b.getHours(), b.getMinutes(), b.getSeconds()
			));
		}
		return adays - bdays + a.getDate() - b.getDate();
	}
}

#if Test
class DateToolsTest extends utest.Test {

	public function testDaysInAYear() {
		utest.Assert.equals(366, DateTools.daysInYear(2024));
		utest.Assert.equals(365, DateTools.daysInYear(2023));
		utest.Assert.equals(365, DateTools.daysInYear(2022));
		utest.Assert.equals(365, DateTools.daysInYear(2021));
		utest.Assert.equals(366, DateTools.daysInYear(2020));
	}

	public function testDaysBetween() {
		utest.Assert.equals(9, DateTools.daysBetween(
			Date.fromString('2024-01-10'),
			Date.fromString('2024-01-01')
		));
		utest.Assert.equals(-31, DateTools.daysBetween(
			Date.fromString('2024-01-01'),
			Date.fromString('2024-02-01')
		));
	}


	public function testBetween() {
		equalsF(-1, DateTools.between(
			Date.fromString("2024-01-01 10:00:00"),
			Date.fromString("2024-01-01 11:00:00"))
		);
		equalsF(0, DateTools.between(
			Date.fromString("2024-01-01 11:00:00"),
			Date.fromString("2024-01-01 11:00:00"))
		);
		equalsF(20/60, DateTools.between(
			Date.fromString("2024-01-01 11:20:00"),
			Date.fromString("2024-01-01 11:00:00"))
		);
		equalsF(24, DateTools.between(
			Date.fromString("2024-01-02 12:00:00"),
			Date.fromString("2024-01-01 12:00:00"))
		);
	}

}
#end
