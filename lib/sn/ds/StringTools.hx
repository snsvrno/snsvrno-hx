package sn.ds;

// is the original haxe `StringTools` class, not this one.
// wonder if there is a better way to reference this while
// keeping the name of this class `StringTools`
using StringTools;

class StringTools {

	/**
		* will split the string into line, is platform agnostic and will
		* correctly split strings encoded on all platforms.
		*
		* uses the `\n` and `\t` split characters
		*/
	public static function lines(string : String) : Array<String> {
		var lines = string.split("\n");
		var i = lines.length-1;
		while (i >= 0) {

			if (lines[i].contains("\t")) {
				var parts = lines[i].split("\t");
				
				for (j in 0 ... parts.length)
					lines.insert(j + i, parts[j]);
			}

			i -= 1;
		}

		// suppose to remove empty elements
		i = lines.length-1;
		while(i >= 0) {
			if (lines[i].length == 0) lines.splice(i,1);
			i -= 1;
		}

		return lines;
	}

	/**
	 * capitalizes the first letter of each word in the string
	 * while lowercasing all the other letters
	 */
	public static function capitalize(text : String) : String {
		var s = text.split(" ");
		for (i in 0 ... s.length) s[i] = s[i].substring(0,1).toUpperCase() + s[i].substring(1).toLowerCase();
		return s.join(" ");
	}

	/**
	 * pads the number with zeros if it is shorter
	 * than the given digits. will not do anything if it
	 * is longer
	 */
	public static function padNumber(number:Int, digits:Int) : String {
		var text = '$number';
		while (text.length < digits) text = "0" + text;
		return text;
	}

}
