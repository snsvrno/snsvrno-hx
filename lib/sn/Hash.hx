package sn;

// pools for the generation, uses `Options` to determine which ones to use
inline var NUMBERS : String = "1234567890";
inline var ALPHA : String = "pyfgcrlsnthdzvwmbqjkxoeuiaPYFGCRLSNTHDZVWMBQJKXOEUIA";

enum abstract Options(Int) to Int {
	var None = 0;
	var AlphaOnly = 1;
}

class Hash {

	/**
	 * generates a basic string of random letters and numbers, is not a true
	 * hash in that there is no way to reference something and make a repeatable
	 * number.
	 *
	 * is intended to be used to creat UUIDs for objects
	 */
	static public function gen(?length : Int = 5, ?options : Options = None) : String {
		var pool : String = ALPHA;

		if (options == None || options % AlphaOnly == 1)
			pool += NUMBERS;

		var hash = "";
		while(hash.length < length) {
			var pos = Math.floor(Math.random() * pool.length);
			hash += pool.charAt(pos);
		}
		return hash;
	}
}
