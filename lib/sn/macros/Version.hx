package sn.macros;

using StringTools;
using sn.ds.StringTools;

class Version {

	/**
	 * loads the content of the file
	 */
	public static macro function load(file:String) : haxe.macro.Expr {
		var array = sys.io.File.getContent(file);
		return macro $v{array};
	}

	/**
	 * reads the git information and builds a version number from that
	 * 
	 * it uses the tag as the version and tacks on the commit if
	 * not exactly on a commit and a "+" if there are unstaged items
	 */
	public static macro function fromGit() : haxe.macro.Expr {
		var currentTag = {
			var process = new sys.io.Process("git", ["describe", "--tags"]);
			var tag = process.stdout.readAll().toString();
			tag.trim().replace("\r","").replace("\n","");
		};

		var modified = {
			var dirty = false;
			var process = new sys.io.Process("git", ["status", "-s"]);
			var text = process.stdout.readAll().toString();
			var files = text.lines();
			for (m in files) {
				if (m.trim().split(" ")[0] == "M") dirty = true;
			}
			dirty;
		}

		var ver = currentTag;
		if (modified) ver += "+";

		return macro $v{ver};
	}
}
