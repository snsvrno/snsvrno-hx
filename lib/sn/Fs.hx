package sn;

using sn.ds.StringTools;

class Fs {
	
	/**
		* recursively gets all the files of a given extension(s) inside a folder.
		*/
	public static function getFilesByExt(path : String, ...ext : String) : Array<String> {
		var found = [];
		for (f in sys.FileSystem.readDirectory(path)) {
			var fullPath = haxe.io.Path.join([path, f]);

			if (sys.FileSystem.isDirectory(fullPath)) {
				for (of in getFilesByExt(fullPath, ... ext)) found.push(of);
			} else {
				var extension = haxe.io.Path.extension(f);
				for (e in ext) if (e == extension) found.push(fullPath);
			}
		}
		return found;
	}

	/**
		* reads a text file and returns the lines
		*/
	public static function contentLines(path : String) : Array<String> {
		var contents = sys.io.File.getContent(path);
		return contents.lines();
	}

	public static function userDirectory() : String {
		var varname = switch(Sys.systemName()) {
			case "Windows": "UserProfile";
			case "Linux": "HOME";
			case "BSD": "HOME";
			case "Mac": "HOME";
			default: throw 'unknown system';
		};
		return Sys.getEnv(varname);
	}

	/**
	 * Makes folder and all parent folders if they don't exist
	 */
	public static function mkdir(path:String) {
		var paths = [];
		var working = path;

		while (working != "") {
			var dir = haxe.io.Path.withoutDirectory(working);
			paths.unshift(dir);
			working = haxe.io.Path.removeTrailingSlashes(path.substring(0, working.length - dir.length));
		}

		if(Sys.systemName() == "Windows" && path.charAt(1) == ":")
			paths[0] = path.split("\\")[0] + paths[0];
		else if (path.charAt(0) == "/") paths[0] = "/" + paths[0];

		for (i in 0 ... paths.length) {
			var path = haxe.io.Path.join(paths.slice(0, i + 1));
			if (!sys.FileSystem.exists(path))
				sys.FileSystem.createDirectory(path);
		}
	}
}
