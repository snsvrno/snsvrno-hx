package;

#if Test
class Test {

	public static function main() {
		utest.UTest.run([
			new sn.ds.Vector2d.Vector2dTest(),
			new sn.ds.Point2d.Point2dTest(),
			new sn.ds.Angle.AngleTest(),
			new sn.ds.ArrayTools.ArrayToolsTest(),
			new sn.ds.DateTools.DateToolsTest(),
			new sn.ds.RomanNumerals.RomanNumeralsTest(),
		]);
	}

	/**
		* helper function that wraps the unit test function.
		*
		* using because i cannot figure out how to have utest
		* use the overriding comparison operatons
		*/
	inline public static function equalsF(f1 : Float, f2 : Float) {
		utest.Assert.equals(true, Math.abs(f1-f2) < 0.0005, 'expected $f1 but it is $f2');
	}

}
#end
